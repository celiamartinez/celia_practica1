﻿using System;
using System.Xml.Schema;

namespace celia_martinez_practica1
{
    class Program
    {
        static void Main(string[] args)
        {
            // ejercicio1();
            //ejercicio2();
            //ejercicio3();
            //ejercicio4();
            //ejercicio5();

        }
        public static void ejercicio1()
        {
            // ingresar 10 notas
            //imprimir cantidad aprobados(>=5) sobresalientes(>=9)

            double nota, aprobados, sobresalientes;
            int total;
            aprobados = 0;
            sobresalientes = 0;
            total = 0;
   
            while (total < 10)
            {
                Console.WriteLine("escribe la nota del alumno");
                nota = double.Parse(Console.ReadLine());
                total++;

                if (nota>=5)
                {
                    sobresalientes++;
                }
            }

            Console.WriteLine("el numero de aprobados es:" + aprobados);
            Console.WriteLine("el numero de sobresalientes es:" + sobresalientes);
        }
        public static void ejercicio2()
        {
            //poner fecha
            //tiene que decir fecha despues de 90 dias
            Console.WriteLine("introduzca una fecha");
            DateTime date1 = DateTime.Parse(Console.ReadLine());
            Console.WriteLine("la fecha actual es:" + date1);
            date1 = date1.AddDays(90);
            Console.WriteLine("la fecha de pago es:" + date1);
        }
        public static void ejercicio3()
        {
           //crear array lista de 10 numeros
           //generar, rellenar, recorrer


            int i;
            int[] num = new int[10];

            num[0] = 12;
            num[1] = 72;
            num[2] = 36;
            num[3] = 44;
            num[4] = 54;
            num[5] = 26;
            num[6] = 15;
            num[7] = 68;
            num[8] = 96;
            num[9] = 80;

            for (i = 0; i < num.Length; i++)
            {
                Console.WriteLine("El numero es " + num[i]);
            }

            while (i > 0)
            {
                i--;
                Console.WriteLine("El numero es " + num[i]);
            }

            public static void ejercicio4()
            {
               //leer datos sobre empresas
               //calcular e indicar peor resultado
               
                string[] empresa = new string[10];
                int[] ingresos = new int[10];
                int[] gastos = new int[10];
                int[] diferencia = new int[10];
                double menor = 0;
                int loc = 0;

                for (int i = 0; i < empresa.Length; i++)
                {
                    empresa[i] = "Empresa" + (i + 1);
                    Console.WriteLine(empresa[i]);
                    Console.WriteLine("introduzca ganancias del mes");

                    ingresos[i] = int.Parse(Console.ReadLine());
                    Console.WriteLine("introduzca gastos del mes");

                    gastos[i] = int.Parse(Console.ReadLine());

                    diferencia[i] = ingresos[i] - gastos[i];

                    if (diferencia[i] < menor)
                    {
                        menor = diferencia[i];
                        loc = i + 1;
                    }
                    Console.WriteLine("");
                }
                Console.WriteLine("Empresa" + loc);
                Console.WriteLine("resultado ingreso-gastos, con una perdida de");
                Console.WriteLine(menor);

            }

            public static void ejercicio5()
            {
                //leer 10 valores numericos
                //imprimir los 2 numeros mas altos

                int num1 = 0;
                int num2 = 0;
                int i = 0;
                int z = 0;
                int[] numeros = new int[10];

                numeros[0] = Int32.Parse(Console.ReadLine());
                numeros[1] = Int32.Parse(Console.ReadLine());
                numeros[2] = Int32.Parse(Console.ReadLine());
                numeros[3] = Int32.Parse(Console.ReadLine());
                numeros[4] = Int32.Parse(Console.ReadLine());
                numeros[5] = Int32.Parse(Console.ReadLine());
                numeros[6] = Int32.Parse(Console.ReadLine());
                numeros[7] = Int32.Parse(Console.ReadLine());
                numeros[8] = Int32.Parse(Console.ReadLine());
                numeros[9] = Int32.Parse(Console.ReadLine());

                while (i < 10)
                {
                    if (numeros[i] > num1)
                    {
                        num1 = numeros[i];
                        z = i;
                    }
                    i++;
                }
                Array.Clear(numeros, z, 1);
                i = 0;

                while (i < 10)
                {
                    if (numeros[i] > num2)
                    {
                        num2 = numeros[i];
                    }
                    i++;
                }
                Console.WriteLine("Los números más altos son: " + num1 + " " + num2);
            }





        }
    }
}
